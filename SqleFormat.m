//
//  SqleFormat.m
//  SQLiteExt
//
//  Created by Dan on 24.01.2022.
//

#import "SqleFormat.h"

@implementation NSString (SqleFormat)

- (NSString *)sqleFormatText {
    g_autofree gchar *ret = sqle_format_text((gchar *)self.UTF8String);
    return NSE_BOX(ret);
}

@end

@implementation NSArray (SqleFormat)

- (NSString *)sqleFormatTexts {
    g_autolist(gchar) sdkSelf = self.gListGchararrayTo;
    g_autofree gchar *ret = sqle_format_texts(sdkSelf);
    return NSE_BOX(ret);
}

- (NSString *)sqleFormatInts {
    g_autoptr(GList) sdkSelf = self.gListGintTo;
    g_autofree gchar *ret = sqle_format_ints(sdkSelf);
    return NSE_BOX(ret);
}

@end
