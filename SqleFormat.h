//
//  SqleFormat.h
//  SQLiteExt
//
//  Created by Dan on 24.01.2022.
//

#import "SqleMain.h"

@interface NSString (SqleFormat)

@property (readonly) NSString *sqleFormatText;

@end

@interface NSArray (SqleFormat)

@property (readonly) NSString *sqleFormatTexts;
@property (readonly) NSString *sqleFormatInts;

@end
